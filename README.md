JP Morgan Interview Test
using Android Architecture Sample
* ViewModel
* LiveData
* Hilt (for dependency injection)
* Kotlin Coroutines
* Retrofit
* Room
* Navigation


You should also take a look at ( SOURCES )
* [Guide to app architecture](https://developer.android.com/topic/architecture)
* [Android architecture samples](https://developer.android.com/topic/architecture)
