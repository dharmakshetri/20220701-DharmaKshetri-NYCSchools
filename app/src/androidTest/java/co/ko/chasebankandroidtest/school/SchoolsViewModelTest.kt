package co.ko.dharmakshetri_nycschools.ui.school

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import co.ko.chasebankandroidtest.ui.school.SchoolsViewModel
import co.ko.chasebankandroidtest.data.local.AppDatabase
import junit.framework.TestCase
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class SchoolsViewModelTest : TestCase() {

    private lateinit var appDatabase: AppDatabase
    private lateinit var viewModel: SchoolsViewModel

    //@get:Rule  //add rule here
    //val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).allowMainThreadQueries().build()

        //val dataSource = SchoolRemoteDataSource(appDatabase.schoolDao())
        //viewModel = SchoolsViewModel(dataSource)
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        appDatabase.close()
    }

    //add test here
}