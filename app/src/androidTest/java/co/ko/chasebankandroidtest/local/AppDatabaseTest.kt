package co.ko.dharmakshetri_nycschools.data.local

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import co.ko.chasebankandroidtest.data.local.AppDatabase
import co.ko.chasebankandroidtest.data.local.SchoolDao
import co.ko.chasebankandroidtest.data.entities.School
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class AppDatabaseTest : TestCase() {

    private lateinit var schoolDao: SchoolDao
    private lateinit var db: AppDatabase

    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        schoolDao = db.schoolDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun writeAndReadSpend() = runBlocking {
        val school = School("Clinton School Writers & Artists, M.S. 260","02M260","Manhattan")
        schoolDao.insert(school)
        val _school = schoolDao.getAllSchools()
        //assertThat(_school.contains(school)).isTrue()
    }

    // we can write more due to time, i can't able to write more test cases
}