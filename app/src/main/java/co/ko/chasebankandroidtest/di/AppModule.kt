package co.ko.chasebankandroidtest.di

import android.content.Context
import co.ko.chasebankandroidtest.BuildConfig.BASE_API_URL
import co.ko.chasebankandroidtest.data.local.AppDatabase
import co.ko.chasebankandroidtest.data.local.SchoolDao
import co.ko.chasebankandroidtest.data.remote.SchoolRemoteDataSource
import co.ko.chasebankandroidtest.data.remote.SchoolService
import co.ko.chasebankandroidtest.data.repository.SchoolRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson) : Retrofit = Retrofit.Builder()
        .baseUrl(BASE_API_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideSchoolService(retrofit: Retrofit): SchoolService = retrofit.create(SchoolService::class.java)

    @Singleton
    @Provides
    fun provideSchoolRemoteDataSource(schoolService: SchoolService) = SchoolRemoteDataSource(schoolService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideSchoolDao(db: AppDatabase) = db.schoolDao()

    @Singleton
    @Provides
    fun provideRepository(remoteDataSource: SchoolRemoteDataSource,
                          localDataSource: SchoolDao
    ) =
        SchoolRepository(remoteDataSource, localDataSource)
}