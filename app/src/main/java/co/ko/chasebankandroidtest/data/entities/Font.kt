package co.ko.chasebankandroidtest.data.entities

data class Font(
    val size: Int
)