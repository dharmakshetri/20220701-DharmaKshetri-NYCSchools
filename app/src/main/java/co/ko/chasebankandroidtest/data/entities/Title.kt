package co.ko.chasebankandroidtest.data.entities

data class Title(
    val attributes: AttributesXX,
    val value: String
)