package co.ko.chasebankandroidtest.data.entities

data class Image(
    val size: Size,
    val url: String
)