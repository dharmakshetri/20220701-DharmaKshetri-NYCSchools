package co.ko.chasebankandroidtest.data.repository

import co.ko.chasebankandroidtest.data.local.SchoolDao
import co.ko.chasebankandroidtest.data.remote.SchoolRemoteDataSource
import co.ko.chasebankandroidtest.utils.performGetOperation
import javax.inject.Inject

class SchoolRepository @Inject constructor(
    private val remoteDataSource: SchoolRemoteDataSource,
    private val localDataSource: SchoolDao
) {

    // in real we have to pass id which has to use and it is optional to choose store data on room, I will give you for
    // for you know in real we do this way to
   /* fun getselectedId(id: Int) = performGetOperation(
        databaseQuery = { localDataSource.getSatDataById(id) },
        networkCall = { remoteDataSource.getSatScores(id) },
        saveCallResult = { localDataSource.insert(it) }
    )*/

    fun getSchools() = performGetOperation(
        databaseQuery = { localDataSource.getAllSchools() },
        networkCall = { remoteDataSource.getSchools() },
        saveCallResult = { localDataSource.insertAll(it) }
    )


    fun getSatScore(dbn: String) = performGetOperation(
        databaseQuery = { localDataSource.getSatDataById(dbn) },
        networkCall = { remoteDataSource.getSatScores() },
        saveCallResult = { localDataSource.insertAllSat(it) }
    )
}