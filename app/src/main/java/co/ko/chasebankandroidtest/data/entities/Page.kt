package co.ko.chasebankandroidtest.data.entities

import co.ko.chasebankandroidtest.data.entities.PageX

data class Page(
    val page: PageX
)