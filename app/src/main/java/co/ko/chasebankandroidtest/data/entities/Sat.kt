package co.ko.chasebankandroidtest.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "sat_score")
data class Sat(
    @PrimaryKey
    val dbn: String,
    @SerializedName("school_name")
    @Expose
    val schoolName: String,
    @SerializedName("num_of_sat_test_takers")
    @Expose
    val  numOfSatTestTakers: String,
    @SerializedName("sat_critical_reading_avg_score")
    @Expose
    val satCriticalReadingAvgScore: String,
    @SerializedName("sat_math_avg_score")
    @Expose
    val satMathAvgScore: String,
    @SerializedName("sat_writing_avg_score")
    @Expose
    val satWritingAvgScore:String

)