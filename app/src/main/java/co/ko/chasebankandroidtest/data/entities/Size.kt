package co.ko.chasebankandroidtest.data.entities

data class Size(
    val height: Int,
    val width: Int
)