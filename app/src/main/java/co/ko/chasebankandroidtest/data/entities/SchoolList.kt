package co.ko.chasebankandroidtest.data.entities

data class SchoolList(
    val info: Info,
    val results: List<School>
)