package co.ko.chasebankandroidtest.data.entities

data class Attributes(
    val font: Font,
    val text_color: String
)