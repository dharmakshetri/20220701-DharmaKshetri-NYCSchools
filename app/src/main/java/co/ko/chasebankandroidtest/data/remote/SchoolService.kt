package co.ko.chasebankandroidtest.data.remote

import co.ko.chasebankandroidtest.data.entities.Sat
import co.ko.chasebankandroidtest.data.entities.School
import retrofit2.Response
import retrofit2.http.GET

interface SchoolService {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolData() : Response<List<School>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSatData() : Response<List<Sat>>

}