package co.ko.chasebankandroidtest.data.remote

import javax.inject.Inject

class SchoolRemoteDataSource @Inject constructor(
    private val schoolService: SchoolService
): BaseDataSource() {
    suspend fun getSchools() = getResult { schoolService.getSchoolData() }
    suspend fun getSatScores() = getResult {
        schoolService.getSatData()
    }
}