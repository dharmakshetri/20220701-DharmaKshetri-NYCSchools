package co.ko.chasebankandroidtest.data.entities

data class PageX(
    val cards: List<Card>
)