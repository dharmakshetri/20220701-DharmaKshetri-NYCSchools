package co.ko.chasebankandroidtest.data.entities

// this basically used for pagination purpose
data class Info(
    val count: Int,
    val next: String,
    val pages: Int,
    val prev: Any
)