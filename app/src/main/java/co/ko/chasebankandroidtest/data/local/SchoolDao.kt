package co.ko.chasebankandroidtest.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.ko.chasebankandroidtest.data.entities.Sat
import co.ko.chasebankandroidtest.data.entities.School
import co.ko.chasebankandroidtest.data.entities.SchoolList

@Dao
interface SchoolDao {

    @Query("SELECT * FROM schools")
    fun getAllSchools() : LiveData<List<School>>

    @Query("SELECT * FROM sat_score WHERE dbn = :dbn") // Actually we have to pass id here
    fun getSatDataById(dbn: String): LiveData<Sat>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(schools: List<School>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllSat(sat: List<Sat>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(school: School)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSat(sat: Sat)



}