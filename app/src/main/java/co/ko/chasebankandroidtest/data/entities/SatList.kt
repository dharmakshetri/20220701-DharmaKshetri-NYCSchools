package co.ko.chasebankandroidtest.data.entities

data class SatList(
    val info: Info,
    val results: List<Sat>
)