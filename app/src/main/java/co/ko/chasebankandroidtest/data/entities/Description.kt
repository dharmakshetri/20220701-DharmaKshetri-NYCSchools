package co.ko.chasebankandroidtest.data.entities

data class Description(
    val attributes: AttributesX,
    val value: String
)