package co.ko.chasebankandroidtest.data.entities

data class Card(
    val card: CardX,
    val card_type: String
)