package co.ko.chasebankandroidtest.data.entities

data class AttributesX(
    val font: FontX,
    val text_color: String
)