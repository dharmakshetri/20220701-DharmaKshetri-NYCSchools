package co.ko.chasebankandroidtest.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "schools")
data class School(
    @SerializedName("school_name")
    @Expose
    val schoolName: String,
    @PrimaryKey
    val dbn: String,
    val city: String
)