package co.ko.chasebankandroidtest.data.entities

data class FontX(
    val size: Int
)