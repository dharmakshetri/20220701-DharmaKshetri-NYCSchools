package co.ko.chasebankandroidtest.ui.school

import androidx.lifecycle.ViewModel
import co.ko.chasebankandroidtest.data.repository.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class SchoolsViewModel @Inject constructor(
    repository: SchoolRepository
) : ViewModel() {
    val schools = repository.getSchools()
}
