package co.ko.chasebankandroidtest.ui.school.ui.sat

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import co.ko.chasebankandroidtest.data.entities.Sat
import co.ko.chasebankandroidtest.data.repository.SchoolRepository
import co.ko.chasebankandroidtest.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SatScoreViewModel @Inject constructor(
    private val repository: SchoolRepository
) : ViewModel() {

    private val _dbn = MutableLiveData<String>()

    private val _sat = _dbn.switchMap { dbn : String ->
        repository.getSatScore(dbn)
    }

    val sat: LiveData<Resource<Sat>> = _sat

    fun start(dbn: String) {
        _dbn.value = dbn
    }

}
