package co.ko.chasebankandroidtest.ui.sat

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import co.ko.chasebankandroidtest.R
import co.ko.chasebankandroidtest.utils.autoCleared
import co.ko.chasebankandroidtest.data.entities.Sat
import co.ko.chasebankandroidtest.databinding.SatScoreFragmentBinding
import co.ko.chasebankandroidtest.ui.school.ui.sat.SatScoreViewModel
import co.ko.chasebankandroidtest.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SatScoreFragment : Fragment() {

    private var binding: SatScoreFragmentBinding by autoCleared()
    private val viewModel: SatScoreViewModel by viewModels()
    private lateinit var dbnn: String
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SatScoreFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString("dbn")?.let { viewModel.start(it) }
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.sat.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    Log.i("DATA","data:dbnn: "+it.data!!)
                    Log.i("DATA","data:message: "+it.status)
                    Log.i("DATA","data:data: "+it.toString())
                    bindSatScore(it.data)
                    binding.progressBar.visibility = View.GONE
                    binding.characterCl.visibility = View.VISIBLE
                }

                Resource.Status.ERROR ->
                    Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.characterCl.visibility = View.GONE
                }
            }
        })
    }

    private fun bindSatScore(sat: Sat) {
        binding.name.text = sat.schoolName
        binding.numOfSatTestTakers.text = getString(R.string.no_of_sat_test)+ sat.numOfSatTestTakers
        binding.satMathAvgScore.text = getString(R.string.math_avg_score)+ sat.satMathAvgScore
        binding.satCriticalReadingAvgScore.text = getString(R.string.reading_avg_score) +sat.satCriticalReadingAvgScore
        binding.satWritingAvgScore.text = getString(R.string.writing_avg_score)+ sat.satWritingAvgScore
    }
}
