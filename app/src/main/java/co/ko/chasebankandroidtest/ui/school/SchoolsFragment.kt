package co.ko.chasebankandroidtest.ui.school

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import co.ko.chasebankandroidtest.R
import co.ko.chasebankandroidtest.databinding.SchoolsFragmentBinding
import co.ko.chasebankandroidtest.ui.school.SchoolsAdapter
import co.ko.chasebankandroidtest.ui.school.SchoolsViewModel
import co.ko.chasebankandroidtest.utils.autoCleared
import co.ko.chasebankandroidtest.utils.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlin.text.Typography.dagger

@AndroidEntryPoint
class SchoolsFragment : Fragment(), SchoolsAdapter.SchoolItemListener {

    private var binding: SchoolsFragmentBinding by autoCleared()
    private val viewModel: SchoolsViewModel by viewModels()
    private lateinit var adapter: SchoolsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SchoolsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    private fun setupRecyclerView() {
        adapter = SchoolsAdapter(this)
        binding.schoolsRv.layoutManager = LinearLayoutManager(requireContext())
        binding.schoolsRv.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.schools.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (!it.data.isNullOrEmpty()) adapter.setItems(ArrayList(it.data))
                }
                Resource.Status.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    Log.i("INFO","issus:"+ it.message)
                }
                Resource.Status.LOADING ->
                    binding.progressBar.visibility = View.VISIBLE
            }
        })
    }

    override fun onClickedSchool(schoolId: String) {
        findNavController().navigate(
            R.id.action_schoolFragment_to_satFragment,
            bundleOf("dbn" to schoolId)
        )
    }
}
