package co.ko.chasebankandroidtest.ui.school

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.ko.chasebankandroidtest.data.entities.School
import co.ko.chasebankandroidtest.databinding.ItemSchoolBinding

class SchoolsAdapter(private val listener: SchoolItemListener) : RecyclerView.Adapter<SchoolViewHolder>() {

    interface SchoolItemListener {
        fun onClickedSchool(schoolId: String)
    }

    private val items = ArrayList<School>()

    fun setItems(items: ArrayList<School>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val binding: ItemSchoolBinding = ItemSchoolBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) = holder.bind(items[position])
}

class SchoolViewHolder(private val itemBinding: ItemSchoolBinding,
                       private val listener: SchoolsAdapter.SchoolItemListener
                       ) : RecyclerView.ViewHolder(itemBinding.root), View.OnClickListener {

    private lateinit var school: School

    init {
        itemBinding.root.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    fun bind(item: School) {
        this.school = item
        itemBinding.name.text = item.schoolName
        itemBinding.schoolCity.text = item.city
       //we can use glide for icon
    }

    override fun onClick(v: View?) {
        Log.i("SCHOOOL","school:dbn= "+school.dbn)
        listener.onClickedSchool(school.dbn)
    }
}

